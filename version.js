#!/usr/bin/env node
const { exec } = require('child_process')
const { getLastCommit } = require('git-last-commit')
const { readFileSync } = require('fs')

getLastCommit((err, { subject, author, branch }) => {
  if (err) {
    console.error(err)
    return
  }
  // if author is bitbucket it will loop
  console.log("Author is:", author.name)
  if (author.name === 'bitbucket-pipelines') return
  if (branch !== 'master') {
    // This is meant mostly for rc deployment but can be applied to any branch
    // If a branch is named rc-0.9.0.0 it will return 'rc-0.9.0.0'
    // If a branch is named QD-10000-silly-name it will return QD-10000
    // This is to keep the prepatch id short
    const taggedBranch = branch.split('-').slice(0, 2).join('-')
    const firstSegment = taggedBranch.split('-')[0]
    const packageJsonVersion = JSON.parse(readFileSync('./package.json').toString()).version
    console.log({ taggedBranch, firstSegment, packageJsonVersion })
    if (packageJsonVersion.includes(firstSegment)) {
      console.log('Pre-releasing, version has already an rc tag')
      exec(`npm version prerelease -m '[skip ci] %s' && git push origin --follow-tags`)
    } else {
      console.log(`Pre-patching, version has now ${branch} tag`)
      exec(`npm version prepatch --preid=${taggedBranch} -m '[skip ci] %s' && git push origin --follow-tags`)
    }
  } else {
    if (/^Merged in feature\//gm.test(subject)) {
      console.log('Versioning as minor', subject, branch, author)
      exec(`npm version minor -m '[skip ci] %s' && git push origin --follow-tags`)
    } else {
      console.log('Versioning as patch', subject, branch, author)
      exec(`npm version patch -m '[skip ci] %s' && git push origin --follow-tags`)
    }
  }
})